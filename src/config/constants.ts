export const METHOD_GET = "GET";
export const METHOD_POST = "POST";
export const METHOD_PATCH = "PATCH";

export const ARCH_MAVEN = "maven";
export const ARCH_NODE = "node";
export const ARCH_ALPINE = "alpine";
export const ARCH_DEBIAN = "debian";
export const ARCH_HELM = "helm";
export const ARCH_GO = "golang";

export const REMOTE_ARCHETYPES: {name: string, stable: boolean}[] = [
	{name: "Generic", stable: true},
	{name: "Maven", stable: true},
	{name: "Node", stable: false},
	{name: "Alpine", stable: true},
	{name: "Debian", stable: false},
	{name: "Helm", stable: false}
];

export const DEFAULT_RESTRICTED_HEADERS = [
	"Authorization",
	"Private-Token",
	"Deploy-Token",
	"Job-Token"
];
