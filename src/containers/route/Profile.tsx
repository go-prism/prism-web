/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import React, {useEffect} from "react";
import {Avatar, makeStyles, Theme, Tooltip, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {useTheme} from "@material-ui/core/styles";
import StandardLayout from "../layout/StandardLayout";
import {getCurrentUser} from "../../store/actions/generic";
import {TState} from "../../store/reducers";
import {GenericState} from "../../store/reducers/generic";
import {getInitials, parseUsername} from "../../utils/parse";

const useStyles = makeStyles((theme: Theme) => ({
	title: {
		fontFamily: "Manrope",
		fontWeight: 500,
		margin: theme.spacing(1)
	}
}));

const Profile: React.FC = (): JSX.Element => {
	// hooks
	const dispatch = useDispatch();
	const classes = useStyles();
	const theme = useTheme();

	// global hooks
	const {currentUser} = useSelector<TState, GenericState>(state => state.generic);

	useEffect(() => {
		dispatch(getCurrentUser());
	}, []);

	return <StandardLayout>
		<div
			style={{display: "flex", alignItems: "center", flexDirection: "column"}}>
			<Avatar
				style={{width: 96, height: 96, backgroundColor: theme.palette.primary.main}}>
				{getInitials(parseUsername(currentUser?.sub || ""))}
			</Avatar>
			<Tooltip title={currentUser?.sub || ""}>
				<Typography
					className={classes.title}
					variant="h3">
					{parseUsername(currentUser?.sub || "")}
				</Typography>
			</Tooltip>
			<Tooltip title={currentUser?.iss || ""}>
				<Typography
					variant="body1">
					{parseUsername(currentUser?.iss || "")}
				</Typography>
			</Tooltip>
		</div>
	</StandardLayout>
}
export default Profile;
