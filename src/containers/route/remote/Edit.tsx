import React, {useEffect, useMemo, useState} from "react";
import {
	Button,
	Chip,
	Collapse,
	FormControlLabel,
	FormGroup,
	FormLabel,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	makeStyles,
	Switch,
	Theme,
	Typography
} from "@material-ui/core";
import Icon from "@mdi/react";
import {mdiPencilOutline, mdiPlusCircleOutline, mdiShapeOutline} from "@mdi/js";
import {useTheme} from "@material-ui/core/styles";
import {Link, useHistory} from "react-router-dom";
import {Code, ValidatedData, ValidatedTextField} from "jmp-coreui";
import {useDispatch, useSelector} from "react-redux";
import Moment from "react-moment";
import {Alert, Skeleton} from "@material-ui/lab";
import {UpdateProfileRequest} from "@prism/prism-rpc/build/gen/service/api/remote_pb";
import {useParams} from "react-router";
import StandardLayout from "../../layout/StandardLayout";
import {DataIsValid} from "../../../utils/data";
import useLoading from "../../../hooks/useLoading";
import {getRemote, REMOTE_GET} from "../../../store/actions/remote/Get";
import {TState} from "../../../store/reducers";
import {DataState} from "../../../store/reducers/data";
import {getRemoteIcon} from "../../../utils/remote";
import {REMOTE_UPDATE, updateRemote} from "../../../store/actions/remote/Update";
import useErrors from "../../../hooks/useErrors";
import getErrorMessage from "../../../selectors/getErrorMessage";
import {resetAction} from "../../../store/actions";
import {RESET_REMOTE} from "../../../store/actions/remote";
import ExpandableListItem from "../../list/ExpandableListItem";
import {CACHE_LIST, listCacheByRemote} from "../../../store/actions/cache/List";
import {RESET_CACHES} from "../../../store/actions/cache";
import {MetadataChip} from "../../../config/types";
import {IDParams} from "../settings";
import CacheList from "./options/CacheList";
import RestrictedHeaders from "./options/RestrictedHeaders";
import FirewallRules from "./options/FirewallRules";
import ClientConfig, {CLIENT_PROFILE_DEFAULT} from "./options/ClientConfig";

const useStyles = makeStyles((theme: Theme) => ({
	title: {
		fontFamily: "Manrope",
		fontWeight: 500
	},
	form: {
		marginTop: theme.spacing(1)
	},
	formItem: {
		margin: theme.spacing(1)
	},
	formIcon: {
		paddingTop: theme.spacing(1.75)
	},
	flex: {
		display: "flex"
	},
	grow: {
		flexGrow: 1
	},
	button: {
		fontFamily: "Manrope",
		fontWeight: 600,
		textTransform: "none"
	},
	buttonDisabled: {
		backgroundColor: theme.palette.action.disabled,
		color: theme.palette.getContrastText(theme.palette.action.disabled)
	},
	chip: {
		margin: theme.spacing(0.5)
	}
}));

const initialURL: ValidatedData = {
	value: "",
	error: "",
	regex: new RegExp(/https?:\/\/(?:w{1,3}\.)?[^\s.]+(?:\.[a-z]+)*(?::\d+)?(?![^<]*(?:<\/\w+>|\/?>))/)
}

const initialName: ValidatedData = {
	value: "",
	error: "",
	regex: new RegExp(/^.{3,}$/)
}

const EditRemote: React.FC = (): JSX.Element => {
	// hooks
	const classes = useStyles();
	const theme = useTheme();
	const dispatch = useDispatch();
	const history = useHistory();
	const {id} = useParams<IDParams>();

	// global state
	const loading = useLoading([REMOTE_GET, REMOTE_UPDATE]);
	const error = useErrors([REMOTE_UPDATE]);
	const errorGet = useErrors([REMOTE_GET]);
	const {remote, cacheEntries} = useSelector<TState, DataState>(state => state.data);

	const loadingCache = useLoading([CACHE_LIST]);
	const errorCache = useErrors([CACHE_LIST]);

	// local state
	const [url, setURL] = useState<ValidatedData>(initialURL);
	const [name, setName] = useState<ValidatedData>(initialName);
	const [success, setSuccess] = useState<boolean>(false);
	const [enabled, setEnabled] = useState<boolean>(false);
	const [stripRestricted, setStripRestricted] = useState<boolean>(false);
	const [resHeaders, setResHeaders] = useState<string[]>([]);
	const [allowList, setAllowList] = useState<string[]>([]);
	const [blockList, setBlockList] = useState<string[]>([]);
	const [profile, setProfile] = useState<UpdateProfileRequest.AsObject>(CLIENT_PROFILE_DEFAULT);
	const [readOnly, setReadOnly] = useState<boolean>(false);

	const open = useMemo(() => {
		return history.location.hash.replace("#", "");
	}, [history.location.hash]);

	useEffect(() => {
		return () => {
			dispatch(resetAction(RESET_REMOTE));
			dispatch(resetAction(RESET_CACHES));
		}
	}, []);

	const chips = useMemo(() => {
		const chipData: MetadataChip[] = [
			{
				label: remote?.archetype || "generic",
				icon: mdiShapeOutline
			},
			{
				label: <Moment fromNow>{remote?.createdAt}</Moment>,
				icon: mdiPlusCircleOutline
			},
			{
				label: <Moment fromNow>{remote?.updatedAt}</Moment>,
				icon: mdiPencilOutline
			},
		];
		return chipData.map(c => <Chip
			className={classes.chip}
			key={c.icon}
			label={c.label}
			icon={<Icon
				path={c.icon}
				size={0.75}
				color={theme.palette.text.secondary}
			/>}
			size="small"
		/>);
	}, [remote]);

	useEffect(() => {
		dispatch(getRemote(id));
	}, [id]);

	useEffect(() => {
		if (remote == null) return;
		setName({...name, value: remote.name});
		setURL({...url, value: remote.uri});
		setEnabled(remote.enabled);
		setStripRestricted(remote.striprestricted);
		setResHeaders(remote.restrictedheadersList || []);
		setAllowList(remote.allowlistList || []);
		setBlockList(remote.blocklistList || []);
		if (remote.clientprofile != null) {
			setProfile({
				skiptls: remote.clientprofile.skiptlsverify,
				ca: remote.clientprofile.tlsca,
				cert: remote.clientprofile.tlscert,
				key: remote.clientprofile.tlskey,
				httpproxy: remote.clientprofile.httpproxy,
				httpsproxy: remote.clientprofile.httpsproxy,
				noproxy: remote.clientprofile.noproxy
			});
		}
		setReadOnly(remote.archetype === "golang");
	}, [remote]);

	const hasChanged = (): boolean => {
		if (remote == null)
			return false;
		if (name.value !== remote.name)
			return true;
		if (url.value !== remote.uri)
			return true;
		if (remote.striprestricted !== stripRestricted)
			return true;
		if (remote.restrictedheadersList !== resHeaders)
			return true;
		if (remote.allowlistList !== allowList)
			return true;
		if (remote.blocklistList !== blockList)
			return true;
		if (remote.clientprofile?.httpsproxy !== profile.httpsproxy)
			return true;
		if (remote.clientprofile?.httpproxy !== profile.httpproxy)
			return true;
		if (remote.clientprofile?.noproxy !== profile.noproxy)
			return true;
		if (remote.clientprofile?.tlsca !== profile.ca)
			return true;
		if (remote.clientprofile?.tlscert !== profile.cert)
			return true;
		if (remote.clientprofile?.tlskey !== profile.key)
			return true;
		if (remote.clientprofile?.skiptlsverify !== profile.skiptls)
			return true;
		return enabled !== remote.enabled;
	};

	const handleUpdate = (): void => {
		if (remote == null) return;
		setSuccess(false);
		dispatch(updateRemote(id, {
			allowlistList: allowList,
			blocklistList: blockList,
			archetype: remote.archetype,
			name: name.value,
			uri: url.value,
			enabled: enabled,
			striprestricted: stripRestricted,
			restrictedheadersList: resHeaders,
			clientprofile: profile
		})).then((action) => {
			// only show the alert if there was a success
			if(action.error !== true) {
				setSuccess(true);
				// reload the remote
				dispatch(getRemote(id));
			}
		});
	}

	const handleOpenCacheList = (): void => {
		if (remote == null) return;
		dispatch(listCacheByRemote(remote.id));
	}

	const handleOpen = (id: string): void => {
		history.push({
			...history.location,
			hash: open === id ? "" : id
		});
	}

	const getOptions = () => {
		const data = [
			{
				id: "cache",
				primary: "Cache",
				secondary: "View cache status and stored data",
				onOpen: handleOpenCacheList,
				children: <CacheList data={cacheEntries} loading={loadingCache} error={errorCache}/>
			},
			{
				id: "firewall-rules",
				primary: "Firewall rules",
				secondary: "Firewall rules define when this remote should be used or skipped.",
				onOpen: null,
				children: <FirewallRules
					allowRules={allowList}
					blockRules={blockList}
					setAllowRules={setAllowList}
					setBlockRules={setBlockList}
					loading={loading}
					disabled={readOnly}
				/>
			},
			{
				id: "restricted-headers",
				primary: "Restricted headers",
				secondary: "Restricted headers control the behaviour of cache partitioning and remote authentication.",
				onOpen: null,
				children: <RestrictedHeaders
					stripRestricted={stripRestricted}
					setStripRestricted={setStripRestricted}
					restrictedHeaders={resHeaders}
					setRestrictedHeaders={setResHeaders}
					loading={loading}
					disabled={readOnly}
				/>
			},
			{
				id: "transport",
				primary: "HTTP/TLS/Proxy options",
				secondary: "Configure how Prism communicates with remotes.",
				onOpen: null,
				children: <ClientConfig
					profile={profile}
					setProfile={p => setProfile({...p})}
					loading={loading}
					disabled={readOnly}
				/>
			}
		];
		return data.map(d => <ExpandableListItem
			key={d.id}
			primary={d.primary}
			secondary={d.secondary}
			open={open === d.id}
			setOpen={o => {
				if (o) {
					d.onOpen?.();
				}
				handleOpen(o ? d.id : "");
			}}>
			{d.children}
		</ExpandableListItem>);
	};

	return (
		<StandardLayout>
			<div>
				{errorGet != null && <Alert
					severity="error">
					Failed to fetch Remote.
					<br/>
					<Code>
						{getErrorMessage(errorGet)}
					</Code>
				</Alert>}
				{success && <Alert
					severity="success">
					Remote updated successfully
				</Alert>}
				<ListItem>
					<ListItemIcon>
						{loading ? <Skeleton variant="circle" animation="wave" width={48} height={48}/> : getRemoteIcon(theme, remote?.archetype || "")}
					</ListItemIcon>
					<ListItemText
						disableTypography
						secondary={<Typography
							color="textSecondary">
							{loading ? <Skeleton animation="wave" width="15%"/> : `Remote ID: ${remote?.id}`}
						</Typography>}>
						<Typography
							className={classes.title}
							color="textPrimary"
							variant="h4">
							{loading ? <Skeleton animation="wave" width="25%" height={64}/> : remote?.name}
						</Typography>
					</ListItemText>
				</ListItem>
				<FormGroup
					className={classes.form}>
					<div>
						{loading ? <Skeleton
							animation="wave"
							width="35%"
							height={32}
						/> : chips}
					</div>
					<FormLabel
						className={classes.formItem}
						component="legend">
						General
					</FormLabel>
					{!loading && readOnly && <Alert
						severity="warning">
						This Remote is read-only and cannot be modified.
					</Alert>}
					<ValidatedTextField
						data={name}
						setData={setName}
						invalidLabel="Must be at least 3 characters."
						fieldProps={{
							className: classes.formItem,
							required: true,
							label: "Remote name",
							variant: "outlined",
							id: "txt-name",
							disabled: loading || readOnly
						}}
					/>
					<ValidatedTextField
						data={url}
						setData={setURL}
						invalidLabel="Must be a valid URL."
						fieldProps={{
							className: classes.formItem,
							required: true,
							label: "Remote URL",
							variant: "outlined",
							id: "txt-url",
							disabled: loading || readOnly
						}}
					/>
					<Collapse
						in={remote != null && !enabled && enabled !== remote?.enabled}>
						<Alert
							className={classes.formItem}
							severity="warning">
							Disabling a remote stops new data from being fetched from it.
							Data that has already been downloaded and cached will still be available.
						</Alert>
					</Collapse>
					<FormControlLabel
						className={classes.formItem}
						control={<Switch
							color="primary"
							checked={enabled}
							disabled={readOnly}
							onChange={(_, checked) => setEnabled(checked)}
						/>}
						label="Enabled"
					/>
					<List>
						{getOptions()}
					</List>
					{error != null && <Alert
						severity="error">
						Failed to update Remote.
						<br/>
						<Code>
							{getErrorMessage(error)}
						</Code>
					</Alert>}
					<div
						className={`${classes.formItem} ${classes.flex}`}>
						<Button
							className={classes.button}
							component={Link}
							to="/settings/remotes"
							variant="outlined">
							Cancel
						</Button>
						<div className={classes.grow}/>
						<Button
							className={classes.button}
							color="primary"
							classes={{
								disabled: classes.buttonDisabled
							}}
							disabled={!DataIsValid(url) || !DataIsValid(name) || loading || !hasChanged() || readOnly}
							onClick={handleUpdate}
							variant="contained">
							Save changes
						</Button>
					</div>
				</FormGroup>
			</div>
		</StandardLayout>
	);
}
export default EditRemote;
