import React, {useEffect, useMemo} from "react";
import {
	Button,
	Divider,
	List,
	ListItem,
	ListItemIcon,
	ListItemSecondaryAction,
	ListItemText,
	makeStyles,
	Theme,
	Tooltip,
	Typography
} from "@material-ui/core";
import {Link} from "react-router-dom";
import Icon from "@mdi/react";
import {mdiCheckCircle, mdiCircle} from "@mdi/js";
import {useTheme} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {Alert} from "@material-ui/lab";
import {ListItemSkeleton} from "jmp-coreui";
import StandardLayout from "../../layout/StandardLayout";
import {TState} from "../../../store/reducers";
import {DataState} from "../../../store/reducers/data";
import {listRemotes, REMOTE_LIST} from "../../../store/actions/remote/List";
import {getRemoteIcon} from "../../../utils/remote";
import {resetAction} from "../../../store/actions";
import {RESET_REMOTES} from "../../../store/actions/remote";
import useLoading from "../../../hooks/useLoading";
import useErrors from "../../../hooks/useErrors";

const useStyles = makeStyles((theme: Theme) => ({
	header: {
		display: "flex",
		marginBottom: theme.spacing(1)
	},
	grow: {
		flexGrow: 1
	},
	button: {
		fontFamily: "Manrope",
		fontWeight: 600,
		fontSize: 13,
		textTransform: "none",
		height: 36
	}
}));

const Remotes: React.FC = (): JSX.Element => {
	// hooks
	const classes = useStyles();
	const theme = useTheme();
	const dispatch = useDispatch();

	// global state
	const {remotes} = useSelector<TState, DataState>(state => state.data);

	const loadingRemotes = useLoading([REMOTE_LIST]);
	const errorRemotes = useErrors([REMOTE_LIST]);

	useEffect(() => {
		dispatch(listRemotes());
		window.document.title = "Remotes";
		return () => {
			dispatch(resetAction(RESET_REMOTES));
		}
	}, []);

	// local state
	const items = useMemo(() => {
		return remotes.map(r => (<ListItem
			button
			component={Link}
			to={`/settings/remotes/${r.id}/-/edit`}
			key={r.id}>
			<ListItemIcon>
				{getRemoteIcon(theme, r.archetype)}
			</ListItemIcon>
			<ListItemText
				secondary={r.uri}>
				{r.name}
			</ListItemText>
			<ListItemSecondaryAction>
				<Tooltip
					title={r.enabled ? "This remote is enabled" : "This remote is disabled"}>
					<Icon
						path={r.enabled ? mdiCheckCircle : mdiCircle}
						size={1}
						color={r.enabled ? theme.palette.success.main : theme.palette.text.secondary}
					/>
				</Tooltip>
			</ListItemSecondaryAction>
		</ListItem>))
	}, [remotes]);

	return (
		<StandardLayout>
			<div
				className={classes.header}>
				<Typography
					variant="h4"
					color="textPrimary">
					Remotes
				</Typography>
				<div className={classes.grow}/>
				<Button
					className={classes.button}
					component={Link}
					disabled={errorRemotes != null}
					to="/remotes/new"
					color="primary"
					variant="contained">
					New remote
				</Button>
			</div>
			<Divider/>
			<List>
				{loadingRemotes && <div>
					<ListItemSkeleton icon/>
					<ListItemSkeleton icon/>
					<ListItemSkeleton icon/>
					<ListItemSkeleton icon/>
				</div>}
				{!loadingRemotes && errorRemotes != null && <Alert
					severity="error">
					Failed to load remotes.
				</Alert>}
				{!loadingRemotes && errorRemotes == null && items.length === 0 && <Alert
					severity="info">
					No remotes
				</Alert>}
				{items}
			</List>
		</StandardLayout>
	);
}
export default Remotes;
