import React, {useEffect, useMemo} from "react";
import {
	Button,
	Divider,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	makeStyles,
	Theme,
	Typography
} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useTheme} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import {Alert} from "@material-ui/lab";
import {ListItemSkeleton} from "jmp-coreui";
import StandardLayout from "../../layout/StandardLayout";
import {TState} from "../../../store/reducers";
import {DataState} from "../../../store/reducers/data";
import {getRemoteIcon} from "../../../utils/remote";
import {listRefracts, REFRACT_LIST} from "../../../store/actions/refract/List";
import {resetAction} from "../../../store/actions";
import {RESET_REFRACTIONS} from "../../../store/actions/refract";
import useLoading from "../../../hooks/useLoading";
import useErrors from "../../../hooks/useErrors";

const useStyles = makeStyles((theme: Theme) => ({
	header: {
		display: "flex",
		marginBottom: theme.spacing(1)
	},
	grow: {
		flexGrow: 1
	},
	button: {
		fontFamily: "Manrope",
		fontWeight: 600,
		fontSize: 13,
		textTransform: "none",
		height: 36
	}
}));

const Refractions: React.FC = (): JSX.Element => {
	// hooks
	const classes = useStyles();
	const theme = useTheme();
	const dispatch = useDispatch();

	// global state
	const {refractions} = useSelector<TState, DataState>(state => state.data);

	const loadingRefracts = useLoading([REFRACT_LIST]);
	const errorRefracts = useErrors([REFRACT_LIST]);

	useEffect(() => {
		dispatch(listRefracts());
		window.document.title = "Refractions";
		return () => {
			dispatch(resetAction(RESET_REFRACTIONS));
		}
	}, []);

	// local state
	const items = useMemo(() => {
		return refractions.map(r => (<ListItem
			button
			component={Link}
			to={`/settings/refract/${r.id}/-/edit`}
			key={r.id}>
			<ListItemIcon>
				{getRemoteIcon(theme, r.archetype)}
			</ListItemIcon>
			<ListItemText>
				{r.name}
			</ListItemText>
		</ListItem>))
	}, [refractions]);

	return (
		<StandardLayout>
			<div
				className={classes.header}>
				<Typography
					variant="h4"
					color="textPrimary">
					Refractions
				</Typography>
				<div className={classes.grow}/>
				<Button
					className={classes.button}
					component={Link}
					disabled={errorRefracts != null}
					to="/refract/new"
					color="primary"
					variant="contained">
					New refraction
				</Button>
			</div>
			<Divider/>
			<List>
				{loadingRefracts && <div>
					<ListItemSkeleton icon/>
					<ListItemSkeleton icon/>
					<ListItemSkeleton icon/>
					<ListItemSkeleton icon/>
				</div>}
				{!loadingRefracts && errorRefracts != null && <Alert
					severity="error">
					Failed to load refractions.
				</Alert>}
				{!loadingRefracts && errorRefracts == null && items.length === 0 && <Alert
					severity="info">
					No refractions
				</Alert>}
				{items}
			</List>
		</StandardLayout>
	);
}
export default Refractions;
