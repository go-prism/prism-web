/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import {RefractActionType} from "./refract";
import {RemoteActionType} from "./remote";
import {CacheActionType} from "./cache";
import {CoreActionType} from "./core";

interface ResetableAction {
	type: string;
}

export const resetAction = (action: string): ResetableAction => {
	return {
		type: action
	};
}

export type DataActionType =
	| CoreActionType
	| CacheActionType
	| RefractActionType
	| RemoteActionType;