import {RSAA, RSAAAction} from "redux-api-middleware";
import {CreateRemoteRequest} from "@prism/prism-rpc/build/gen/service/api/remote_pb";
import {METHOD_POST} from "../../../config/constants";
import {API_URL} from "../../../config";
import {RemoteV1} from "../../../config/types";

export const REMOTE_CREATE = "REMOTE_CREATE";
export const REMOTE_CREATE_REQUEST = "REMOTE_CREATE_REQUEST";
export const REMOTE_CREATE_SUCCESS = "REMOTE_CREATE_SUCCESS";
export const REMOTE_CREATE_FAILURE = "REMOTE_CREATE_FAILURE";

interface RemoteCreateRequestActionType {
	type: typeof REMOTE_CREATE_REQUEST;
}

interface RemoteCreateSuccessActionType {
	type: typeof REMOTE_CREATE_SUCCESS;
	payload: RemoteV1;
}

interface RemoteCreateFailureActionType {
	type: typeof REMOTE_CREATE_FAILURE;
	error: Error;
}

export const createRemote = (i: CreateRemoteRequest.AsObject): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_POST,
			endpoint: `${API_URL}/api/v1/remote`,
			body: JSON.stringify(i),
			headers: {
				"Content-Type": "application/json"
			},
			types: [REMOTE_CREATE_REQUEST, REMOTE_CREATE_SUCCESS, REMOTE_CREATE_FAILURE]
		}
	};
}

export type RemoteCreateActionType =
	RemoteCreateRequestActionType |
	RemoteCreateSuccessActionType |
	RemoteCreateFailureActionType;