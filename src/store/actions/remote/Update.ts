import {RSAA, RSAAAction} from "redux-api-middleware";
import {CreateRemoteRequest} from "@prism/prism-rpc/build/gen/service/api/remote_pb";
import {METHOD_PATCH} from "../../../config/constants";
import {API_URL} from "../../../config";

export const REMOTE_UPDATE = "REMOTE_UPDATE";
export const REMOTE_UPDATE_REQUEST = "REMOTE_UPDATE_REQUEST";
export const REMOTE_UPDATE_SUCCESS = "REMOTE_UPDATE_SUCCESS";
export const REMOTE_UPDATE_FAILURE = "REMOTE_UPDATE_FAILURE";

interface RemoteUpdateRequestActionType {
	type: typeof REMOTE_UPDATE_REQUEST;
}

interface RemoteUpdateSuccessActionType {
	type: typeof REMOTE_UPDATE_SUCCESS;
	payload: void;
}

interface RemoteUpdateFailureActionType {
	type: typeof REMOTE_UPDATE_FAILURE;
	error: Error;
}

export const updateRemote = (id: string, i: CreateRemoteRequest.AsObject): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_PATCH,
			endpoint: `${API_URL}/api/v1/remote/${id}`,
			body: JSON.stringify(i),
			headers: {
				"Content-Type": "application/json"
			},
			types: [REMOTE_UPDATE_REQUEST, REMOTE_UPDATE_SUCCESS, REMOTE_UPDATE_FAILURE]
		}
	};
}

export type RemoteUpdateActionType =
	RemoteUpdateRequestActionType |
	RemoteUpdateSuccessActionType |
	RemoteUpdateFailureActionType;