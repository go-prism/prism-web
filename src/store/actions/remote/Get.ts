import {RSAA, RSAAAction} from "redux-api-middleware";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";
import {RemoteV1} from "../../../config/types";

export const REMOTE_GET = "REMOTE_GET";
export const REMOTE_GET_REQUEST = "REMOTE_GET_REQUEST";
export const REMOTE_GET_SUCCESS = "REMOTE_GET_SUCCESS";
export const REMOTE_GET_FAILURE = "REMOTE_GET_FAILURE";

interface RemoteGetRequestActionType {
	type: typeof REMOTE_GET_REQUEST;
}

interface RemoteGetSuccessActionType {
	type: typeof REMOTE_GET_SUCCESS;
	payload: RemoteV1;
}

interface RemoteGetFailureActionType {
	type: typeof REMOTE_GET_FAILURE;
	error: Error;
}

export const getRemote = (id: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/remote/${id}`,
			types: [REMOTE_GET_REQUEST, REMOTE_GET_SUCCESS, REMOTE_GET_FAILURE]
		}
	};
}

export type RemoteGetActionType =
	RemoteGetRequestActionType |
	RemoteGetSuccessActionType |
	RemoteGetFailureActionType;