import {RemoteListActionType} from "./List";
import {RemoteCreateActionType} from "./Create";
import {RemoteGetActionType} from "./Get";
import {RemoteUpdateActionType} from "./Update";
import {RemoteListArchActionType} from "./ListByArch";

export const RESET_REMOTE = "RESET_REMOTE";
export const RESET_REMOTES = "RESET_REMOTES";

export interface ResetRemoteActionType {
	type: typeof RESET_REMOTE;
}

export interface ResetRemotesActionType {
	type: typeof RESET_REMOTES;
}

export type RemoteActionType =
	| ResetRemoteActionType
	| ResetRemotesActionType
	| RemoteListArchActionType
	| RemoteListActionType
	| RemoteGetActionType
	| RemoteUpdateActionType
	| RemoteCreateActionType;