import {RSAA, RSAAAction} from "redux-api-middleware";
import {Remotes} from "@prism/prism-rpc";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";

export const REMOTE_LIST_ARCH = "REMOTE_LIST_ARCH";
export const REMOTE_LIST_ARCH_REQUEST = "REMOTE_LIST_ARCH_REQUEST";
export const REMOTE_LIST_ARCH_SUCCESS = "REMOTE_LIST_ARCH_SUCCESS";
export const REMOTE_LIST_ARCH_FAILURE = "REMOTE_LIST_ARCH_FAILURE";

interface RemoteListArchRequestActionType {
	type: typeof REMOTE_LIST_ARCH_REQUEST;
}

interface RemoteListArchSuccessActionType {
	type: typeof REMOTE_LIST_ARCH_SUCCESS;
	payload: Remotes.AsObject;
}

interface RemoteListArchFailureActionType {
	type: typeof REMOTE_LIST_ARCH_FAILURE;
	error: Error;
}

export const listRemotesByArch = (arch: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/remote/-/arch?arch=${arch}`,
			types: [REMOTE_LIST_ARCH_REQUEST, REMOTE_LIST_ARCH_SUCCESS, REMOTE_LIST_ARCH_FAILURE]
		}
	};
}

export type RemoteListArchActionType =
	RemoteListArchRequestActionType |
	RemoteListArchSuccessActionType |
	RemoteListArchFailureActionType;