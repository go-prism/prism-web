import {RSAA, RSAAAction} from "redux-api-middleware";
import {Remotes} from "@prism/prism-rpc";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";

export const REMOTE_LIST = "REMOTE_LIST";
export const REMOTE_LIST_REQUEST = "REMOTE_LIST_REQUEST";
export const REMOTE_LIST_SUCCESS = "REMOTE_LIST_SUCCESS";
export const REMOTE_LIST_FAILURE = "REMOTE_LIST_FAILURE";

interface RemoteListRequestActionType {
	type: typeof REMOTE_LIST_REQUEST;
}

interface RemoteListSuccessActionType {
	type: typeof REMOTE_LIST_SUCCESS;
	payload: Remotes.AsObject;
}

interface RemoteListFailureActionType {
	type: typeof REMOTE_LIST_FAILURE;
	error: Error;
}

export const listRemotes = (): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/remote`,
			types: [REMOTE_LIST_REQUEST, REMOTE_LIST_SUCCESS, REMOTE_LIST_FAILURE]
		}
	};
}

export type RemoteListActionType =
	RemoteListRequestActionType |
	RemoteListSuccessActionType |
	RemoteListFailureActionType;