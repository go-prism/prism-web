/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import {RSAA, RSAAAction} from "redux-api-middleware";
import {CacheEntries} from "@prism/prism-rpc";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";

export const CACHE_LIST = "CACHE_LIST";
export const CACHE_LIST_REQUEST = "CACHE_LIST_REQUEST";
export const CACHE_LIST_SUCCESS = "CACHE_LIST_SUCCESS";
export const CACHE_LIST_FAILURE = "CACHE_LIST_FAILURE";

interface CacheListRequestActionType {
	type: typeof CACHE_LIST_REQUEST;
}

interface CacheListSuccessActionType {
	type: typeof CACHE_LIST_SUCCESS;
	payload: CacheEntries.AsObject;
}

interface CacheListFailureActionType {
	type: typeof CACHE_LIST_FAILURE;
	error: Error;
}

export const listCacheByRemote = (remoteID: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/cache-entry/-/remote/${remoteID}`,
			types: [CACHE_LIST_REQUEST, CACHE_LIST_SUCCESS, CACHE_LIST_FAILURE]
		}
	};
}

export const listCacheByRefract = (refractID: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/cache-entry/-/refract/${refractID}`,
			types: [CACHE_LIST_REQUEST, CACHE_LIST_SUCCESS, CACHE_LIST_FAILURE]
		}
	};
}

export type CacheListActionType =
	CacheListRequestActionType |
	CacheListSuccessActionType |
	CacheListFailureActionType;