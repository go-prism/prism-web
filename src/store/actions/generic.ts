import {RSAA, RSAAAction} from "redux-api-middleware";
import {ReactorStatuses} from "@prism/prism-rpc/build/gen/domain/v1/reactor_pb";
import {METHOD_GET} from "../../config/constants";
import {API_URL} from "../../config";
import {User} from "../../config/types";

export const SET_THEME_MODE = "SET_THEME_MODE";
export const GET_OIDC = "GET_OIDC";
export const GET_OIDC_REQUEST = "GET_OIDC_REQUEST";
export const GET_OIDC_SUCCESS = "GET_OIDC_SUCCESS";
export const GET_OIDC_FAILURE = "GET_OIDC_FAILURE";

export const GET_REACTOR_STATUS = "GET_REACTOR_STATUS";
export const GET_REACTOR_STATUS_REQUEST = "GET_REACTOR_STATUS_REQUEST";
export const GET_REACTOR_STATUS_SUCCESS = "GET_REACTOR_STATUS_SUCCESS";
export const GET_REACTOR_STATUS_FAILURE = "GET_REACTOR_STATUS_FAILURE";

export const GET_CURRENT_USER = "GET_CURRENT_USER";
export const GET_CURRENT_USER_REQUEST = "GET_CURRENT_USER_REQUEST";
export const GET_CURRENT_USER_SUCCESS = "GET_CURRENT_USER_SUCCESS";
export const GET_CURRENT_USER_FAILURE = "GET_CURRENT_USER_FAILURE";

interface SetThemeModeActionType {
	type: typeof SET_THEME_MODE;
	payload: string;
}

interface GetOIDCRequestActionType {
	type: typeof GET_OIDC_REQUEST;
}

interface GetOIDCSuccessActionType {
	type: typeof GET_OIDC_SUCCESS;
}

interface GetOIDCFailureActionType {
	type: typeof GET_OIDC_FAILURE;
}

interface GetReactorStatusRequestActionType {
	type: typeof GET_REACTOR_STATUS_REQUEST;
}

interface GetReactorStatusSuccessActionType {
	type: typeof GET_REACTOR_STATUS_SUCCESS;
	payload: ReactorStatuses.AsObject;
}

interface GetReactorStatusFailureActionType {
	type: typeof GET_REACTOR_STATUS_FAILURE;
	error: Error;
}

interface GetCurrentUserRequestActionType {
	type: typeof GET_CURRENT_USER_REQUEST;
}

interface GetCurrentUserSuccessActionType {
	type: typeof GET_CURRENT_USER_SUCCESS;
	payload: User;
}

interface GetCurrentUserFailureActionType {
	type: typeof GET_CURRENT_USER_FAILURE;
	error: Error;
}

export const setThemeMode = (theme: string): SetThemeModeActionType => {
	return {
		type: SET_THEME_MODE,
		payload: theme
	};
};

export const getOIDC = (): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/auth/ping`,
			types: [GET_OIDC_REQUEST, GET_OIDC_SUCCESS, GET_OIDC_FAILURE]
		}
	}
}

export const getCurrentUser = (): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/user/-/me`,
			types: [GET_CURRENT_USER_REQUEST, GET_CURRENT_USER_SUCCESS, GET_CURRENT_USER_FAILURE]
		}
	}
}

export const getReactorStatus = (): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/reactor/-/status`,
			types: [GET_REACTOR_STATUS_REQUEST, GET_REACTOR_STATUS_SUCCESS, GET_REACTOR_STATUS_FAILURE]
		}
	}
}

export type GenericActionType =
	| GetCurrentUserRequestActionType
	| GetCurrentUserSuccessActionType
	| GetCurrentUserFailureActionType
	| SetThemeModeActionType
	| GetOIDCRequestActionType
	| GetOIDCSuccessActionType
	| GetOIDCFailureActionType
	| GetReactorStatusRequestActionType
	| GetReactorStatusSuccessActionType
	| GetReactorStatusFailureActionType;
