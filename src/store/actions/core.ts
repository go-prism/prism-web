import {Dispatch} from "redux";
import {API_URL} from "../../config";

export const GET_FILE = "GET_FILE";
export const GET_FILE_REQUEST = "GET_FILE_REQUEST";
export const GET_FILE_SUCCESS = "GET_FILE_SUCCESS";
export const GET_FILE_FAILURE = "GET_FILE_FAILURE";

export const GET_FILE_RESET = "GET_FILE_RESET";

interface GetFileRequestActionType {
	type: typeof GET_FILE_REQUEST | typeof GET_FILE_RESET;
}

interface GetFileSuccessActionType {
	type: typeof GET_FILE_SUCCESS;
	payload: string;
}

interface GetFileFailureActionType {
	type: typeof GET_FILE_FAILURE;
	error: Error;
}

export const getFile = (dispatch: Dispatch, refraction: string, path: string): void => {
	dispatch({type: GET_FILE_REQUEST});
	fetch(`${API_URL}/api/-/${refraction}/${path}`)
		.then((res: Response) => {
			if (!res.ok) {
				throw new Error(res.statusText);
			}
			return res.text();
		})
		.then((str: string) => {
			dispatch({type: GET_FILE_SUCCESS, payload: str});
		})
		.catch((e: Error) => dispatch({type: GET_FILE_FAILURE, error: e}));
}

export type CoreActionType =
	| GetFileRequestActionType
	| GetFileSuccessActionType
	| GetFileFailureActionType;
