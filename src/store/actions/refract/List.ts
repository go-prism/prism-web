import {RSAA, RSAAAction} from "redux-api-middleware";
import {Refractions} from "@prism/prism-rpc";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";

export const REFRACT_LIST = "REFRACT_LIST";
export const REFRACT_LIST_REQUEST = "REFRACT_LIST_REQUEST";
export const REFRACT_LIST_SUCCESS = "REFRACT_LIST_SUCCESS";
export const REFRACT_LIST_FAILURE = "REFRACT_LIST_FAILURE";

interface RefractListRequestActionType {
	type: typeof REFRACT_LIST_REQUEST;
}

interface RefractListSuccessActionType {
	type: typeof REFRACT_LIST_SUCCESS;
	payload: Refractions.AsObject;
}

interface RefractListFailureActionType {
	type: typeof REFRACT_LIST_FAILURE;
	error: Error;
}

export const listRefracts = (): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/refract`,
			types: [REFRACT_LIST_REQUEST, REFRACT_LIST_SUCCESS, REFRACT_LIST_FAILURE]
		}
	};
}

export type RefractListActionType =
	RefractListRequestActionType |
	RefractListSuccessActionType |
	RefractListFailureActionType;