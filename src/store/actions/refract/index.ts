import {RefractListActionType} from "./List";
import {RefractCreateActionType} from "./Create";
import {RefractGetActionType} from "./Get";
import {RefractUpdateActionType} from "./Update";
import {RefractGetInfoActionType} from "./GetInfo";

export const RESET_REFRACTION = "RESET_REFRACTION";
export const RESET_REFRACTIONS = "RESET_REFRACTIONS";

export interface ResetRefractionActionType {
	type: typeof RESET_REFRACTION;
}

export interface ResetRefractionsActionType {
	type: typeof RESET_REFRACTIONS;
}

export type RefractActionType =
	| ResetRefractionActionType
	| ResetRefractionsActionType
	| RefractListActionType
	| RefractGetActionType
	| RefractGetInfoActionType
	| RefractUpdateActionType
	| RefractCreateActionType;