import {RSAA, RSAAAction} from "redux-api-middleware";
import {GetRefractInfoResponse} from "@prism/prism-rpc/build/gen/service/reactor/api_pb";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";

export const REFRACT_GET_INFO = "REFRACT_GET_INFO";
export const REFRACT_GET_INFO_REQUEST = "REFRACT_GET_INFO_REQUEST";
export const REFRACT_GET_INFO_SUCCESS = "REFRACT_GET_INFO_SUCCESS";
export const REFRACT_GET_INFO_FAILURE = "REFRACT_GET_INFO_FAILURE";

interface RefractGetInfoRequestActionType {
	type: typeof REFRACT_GET_INFO_REQUEST;
}

interface RefractGetInfoSuccessActionType {
	type: typeof REFRACT_GET_INFO_SUCCESS;
	payload: GetRefractInfoResponse.AsObject;
}

interface RefractGetInfoFailureActionType {
	type: typeof REFRACT_GET_INFO_FAILURE;
	error: Error;
}

export const getRefractInfo = (id: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/refract/${id}/-/info`,
			types: [REFRACT_GET_INFO_REQUEST, REFRACT_GET_INFO_SUCCESS, REFRACT_GET_INFO_FAILURE]
		}
	};
}

export type RefractGetInfoActionType =
	RefractGetInfoRequestActionType |
	RefractGetInfoSuccessActionType |
	RefractGetInfoFailureActionType;