import {RSAA, RSAAAction} from "redux-api-middleware";
import {CreateRefractRequest} from "@prism/prism-rpc/build/gen/service/api/refract_pb";
import {METHOD_PATCH} from "../../../config/constants";
import {API_URL} from "../../../config";

export const REFRACT_UPDATE = "REFRACT_UPDATE";
export const REFRACT_UPDATE_REQUEST = "REFRACT_UPDATE_REQUEST";
export const REFRACT_UPDATE_SUCCESS = "REFRACT_UPDATE_SUCCESS";
export const REFRACT_UPDATE_FAILURE = "REFRACT_UPDATE_FAILURE";

interface RefractUpdateRequestActionType {
	type: typeof REFRACT_UPDATE_REQUEST;
}

interface RefractUpdateSuccessActionType {
	type: typeof REFRACT_UPDATE_SUCCESS;
	payload: void;
}

interface RefractUpdateFailureActionType {
	type: typeof REFRACT_UPDATE_FAILURE;
	error: Error;
}

export const updateRefract = (id: string, i: CreateRefractRequest.AsObject): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_PATCH,
			endpoint: `${API_URL}/api/v1/refract/${id}`,
			body: JSON.stringify(i),
			headers: {
				"Content-Type": "application/json"
			},
			types: [REFRACT_UPDATE_REQUEST, REFRACT_UPDATE_SUCCESS, REFRACT_UPDATE_FAILURE]
		}
	};
}

export type RefractUpdateActionType =
	RefractUpdateRequestActionType |
	RefractUpdateSuccessActionType |
	RefractUpdateFailureActionType;