import {RSAA, RSAAAction} from "redux-api-middleware";
import {CreateRefractRequest} from "@prism/prism-rpc/build/gen/service/api/refract_pb";
import {METHOD_POST} from "../../../config/constants";
import {API_URL} from "../../../config";
import {RefractionV1} from "../../../config/types";

export const REFRACT_CREATE = "REFRACT_CREATE";
export const REFRACT_CREATE_REQUEST = "REFRACT_CREATE_REQUEST";
export const REFRACT_CREATE_SUCCESS = "REFRACT_CREATE_SUCCESS";
export const REFRACT_CREATE_FAILURE = "REFRACT_CREATE_FAILURE";

interface RefractCreateRequestActionType {
	type: typeof REFRACT_CREATE_REQUEST;
}

interface RefractCreateSuccessActionType {
	type: typeof REFRACT_CREATE_SUCCESS;
	payload: RefractionV1;
}

interface RefractCreateFailureActionType {
	type: typeof REFRACT_CREATE_FAILURE;
	error: Error;
}

export const createRefract = (i: CreateRefractRequest.AsObject): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_POST,
			endpoint: `${API_URL}/api/v1/refract`,
			body: JSON.stringify(i),
			headers: {
				"Content-Type": "application/json"
			},
			types: [REFRACT_CREATE_REQUEST, REFRACT_CREATE_SUCCESS, REFRACT_CREATE_FAILURE]
		}
	};
}

export type RefractCreateActionType =
	RefractCreateRequestActionType |
	RefractCreateSuccessActionType |
	RefractCreateFailureActionType;