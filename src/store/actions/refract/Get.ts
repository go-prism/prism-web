import {RSAA, RSAAAction} from "redux-api-middleware";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";
import {RefractionV1} from "../../../config/types";

export const REFRACT_GET = "REFRACT_GET";
export const REFRACT_GET_REQUEST = "REFRACT_GET_REQUEST";
export const REFRACT_GET_SUCCESS = "REFRACT_GET_SUCCESS";
export const REFRACT_GET_FAILURE = "REFRACT_GET_FAILURE";

interface RefractGetRequestActionType {
	type: typeof REFRACT_GET_REQUEST;
}

interface RefractGetSuccessActionType {
	type: typeof REFRACT_GET_SUCCESS;
	payload: RefractionV1;
}

interface RefractGetFailureActionType {
	type: typeof REFRACT_GET_FAILURE;
	error: Error;
}

export const getRefract = (id: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/refract/${id}`,
			types: [REFRACT_GET_REQUEST, REFRACT_GET_SUCCESS, REFRACT_GET_FAILURE]
		}
	};
}

export type RefractGetActionType =
	RefractGetRequestActionType |
	RefractGetSuccessActionType |
	RefractGetFailureActionType;