import {RSAA, RSAAAction} from "redux-api-middleware";
import {HostRewrite} from "@prism/prism-rpc/build/gen/domain/archv1/golang_pb";
import {METHOD_POST} from "../../../config/constants";
import {API_URL} from "../../../config";

export const HOST_RULE_CREATE = "HOST_RULE_CREATE";
export const HOST_RULE_CREATE_REQUEST = "HOST_RULE_CREATE_REQUEST";
export const HOST_RULE_CREATE_SUCCESS = "HOST_RULE_CREATE_SUCCESS";
export const HOST_RULE_CREATE_FAILURE = "HOST_RULE_CREATE_FAILURE";

interface HostRuleCreateRequestActionType {
	type: typeof HOST_RULE_CREATE_REQUEST;
}

interface HostRuleCreateSuccessActionType {
	type: typeof HOST_RULE_CREATE_SUCCESS;
	payload: HostRewrite.AsObject;
}

interface HostRuleCreateFailureActionType {
	type: typeof HOST_RULE_CREATE_FAILURE;
	error: Error;
}

export const createHostRule = (source: string, destination: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_POST,
			endpoint: `${API_URL}/api/v1/gorule`,
			body: JSON.stringify({
				source,
				destination
			}),
			headers: {
				"Content-Type": "application/json"
			},
			types: [HOST_RULE_CREATE_REQUEST, HOST_RULE_CREATE_SUCCESS, HOST_RULE_CREATE_FAILURE]
		}
	};
}

export type HostRuleCreateActionType =
	HostRuleCreateRequestActionType |
	HostRuleCreateSuccessActionType |
	HostRuleCreateFailureActionType;