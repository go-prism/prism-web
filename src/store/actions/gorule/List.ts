import {RSAA, RSAAAction} from "redux-api-middleware";
import {HostRewrites} from "@prism/prism-rpc/build/gen/domain/archv1/golang_pb";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";

export const HOST_RULE_LIST = "HOST_RULE_LIST";
export const HOST_RULE_LIST_REQUEST = "HOST_RULE_LIST_REQUEST";
export const HOST_RULE_LIST_SUCCESS = "HOST_RULE_LIST_SUCCESS";
export const HOST_RULE_LIST_FAILURE = "HOST_RULE_LIST_FAILURE";

interface HostRuleListRequestActionType {
	type: typeof HOST_RULE_LIST_REQUEST;
}

interface HostRuleListSuccessActionType {
	type: typeof HOST_RULE_LIST_SUCCESS;
	payload: HostRewrites.AsObject;
}

interface HostRuleListFailureActionType {
	type: typeof HOST_RULE_LIST_FAILURE;
	error: Error;
}

export const listHostRule = (): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/gorule`,
			types: [HOST_RULE_LIST_REQUEST, HOST_RULE_LIST_SUCCESS, HOST_RULE_LIST_FAILURE]
		}
	};
}

export type HostRuleListActionType =
	HostRuleListRequestActionType |
	HostRuleListSuccessActionType |
	HostRuleListFailureActionType;