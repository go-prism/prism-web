import {HostRuleCreateActionType} from "./Create";
import {HostRuleListActionType} from "./List";

export const RESET_HOST_RULES = "RESET_HOST_RULES";

export interface ResetHostRulesActionType {
	type: typeof RESET_HOST_RULES;
}

export type HostRuleActionType =
	| ResetHostRulesActionType
	| HostRuleCreateActionType
	| HostRuleListActionType;
