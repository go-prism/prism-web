import {RSAA, RSAAAction} from "redux-api-middleware";
import {RoleBindings} from "@prism/prism-rpc/build/gen/domain/v1/roles_pb";
import {METHOD_POST} from "../../../config/constants";
import {API_URL} from "../../../config";

export const ROLE_CREATE = "ROLE_CREATE";
export const ROLE_CREATE_REQUEST = "ROLE_CREATE_REQUEST";
export const ROLE_CREATE_SUCCESS = "ROLE_CREATE_SUCCESS";
export const ROLE_CREATE_FAILURE = "ROLE_CREATE_FAILURE";

interface RoleCreateRequestActionType {
	type: typeof ROLE_CREATE_REQUEST;
}

interface RoleCreateSuccessActionType {
	type: typeof ROLE_CREATE_SUCCESS;
	payload: RoleBindings.AsObject;
}

interface RoleCreateFailureActionType {
	type: typeof ROLE_CREATE_FAILURE;
	error: Error;
}

export const createRole = (name: string, subject: string, username: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_POST,
			endpoint: `${API_URL}/api/v1/auth/-/role`,
			body: JSON.stringify({
				name,
				subject,
				username
			}),
			types: [ROLE_CREATE_REQUEST, ROLE_CREATE_SUCCESS, ROLE_CREATE_FAILURE]
		}
	};
}

export type RoleCreateActionType =
	RoleCreateRequestActionType |
	RoleCreateSuccessActionType |
	RoleCreateFailureActionType;
