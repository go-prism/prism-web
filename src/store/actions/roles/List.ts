import {RSAA, RSAAAction} from "redux-api-middleware";
import {RoleBindings} from "@prism/prism-rpc/build/gen/domain/v1/roles_pb";
import {METHOD_GET} from "../../../config/constants";
import {API_URL} from "../../../config";

export const ROLE_LIST = "ROLE_LIST";
export const ROLE_LIST_REQUEST = "ROLE_LIST_REQUEST";
export const ROLE_LIST_SUCCESS = "ROLE_LIST_SUCCESS";
export const ROLE_LIST_FAILURE = "ROLE_LIST_FAILURE";

interface RoleListRequestActionType {
	type: typeof ROLE_LIST_REQUEST;
}

interface RoleListSuccessActionType {
	type: typeof ROLE_LIST_SUCCESS;
	payload: RoleBindings.AsObject;
}

interface RoleListFailureActionType {
	type: typeof ROLE_LIST_FAILURE;
	error: Error;
}

export const listRoles = (user?: string): RSAAAction => {
	return {
		[RSAA]: {
			method: METHOD_GET,
			endpoint: `${API_URL}/api/v1/auth/-/roles?user=${user || ""}`,
			types: [ROLE_LIST_REQUEST, ROLE_LIST_SUCCESS, ROLE_LIST_FAILURE]
		}
	};
}

export type RoleListActionType =
	RoleListRequestActionType |
	RoleListSuccessActionType |
	RoleListFailureActionType;
