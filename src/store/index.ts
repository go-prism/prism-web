import {applyMiddleware, compose, createStore} from "redux";
import {apiMiddleware} from "redux-api-middleware";
import rootReducers from "./reducers";

declare global {
	interface Window {
		__REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose
	}
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(rootReducers, composeEnhancers(applyMiddleware(apiMiddleware)));
