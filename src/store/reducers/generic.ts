import {ReactorStatuses} from "@prism/prism-rpc/build/gen/domain/v1/reactor_pb";
import {
	GenericActionType,
	GET_CURRENT_USER_SUCCESS,
	GET_OIDC_FAILURE,
	GET_OIDC_SUCCESS,
	GET_REACTOR_STATUS_SUCCESS,
	SET_THEME_MODE
} from "../actions/generic";
import {User} from "../../config/types";

export interface GenericState {
	themeMode: string;
	oidcEnabled: boolean;
	reactorStatus: ReactorStatuses.AsObject | null;
	currentUser: User | null;
}

const wantedTheme = (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) ? "dark" : "light";

const initialState: GenericState = {
	themeMode: wantedTheme,
	oidcEnabled: false, // assume false until we can confirm
	reactorStatus: null,
	currentUser: null
};

const generic = (state = initialState, action: GenericActionType): GenericState => {
	switch (action.type) {
		case GET_OIDC_SUCCESS:
			return {...state, oidcEnabled: true};
		case GET_OIDC_FAILURE:
			return {...state, oidcEnabled: false};
		case SET_THEME_MODE:
			return {...state, themeMode: action.payload};
		case GET_REACTOR_STATUS_SUCCESS:
			return {...state, reactorStatus: action.payload};
		case GET_CURRENT_USER_SUCCESS:
			return {...state, currentUser: action.payload};
		default:
			return state;
	}
};
export default generic;
