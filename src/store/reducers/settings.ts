import {HostRewrite} from "@prism/prism-rpc/build/gen/domain/archv1/golang_pb";
import {RoleBinding} from "@prism/prism-rpc/build/gen/domain/v1/roles_pb";
import {HostRuleActionType, RESET_HOST_RULES} from "../actions/gorule";
import {HOST_RULE_LIST_SUCCESS} from "../actions/gorule/List";
import {RoleActionType} from "../actions/roles";
import {ROLE_LIST_SUCCESS} from "../actions/roles/List";

export interface SettingsState {
	rules: Array<HostRewrite.AsObject>;
	roles: Array<RoleBinding.AsObject>;
}

const initialState: SettingsState = {
	rules: [],
	roles: []
};

const settings = (state = initialState, action: HostRuleActionType | RoleActionType): SettingsState => {
	switch (action.type) {
		case RESET_HOST_RULES:
			return {...state, rules: []};
		case HOST_RULE_LIST_SUCCESS:
			return {...state, rules: action.payload.rulesList};
		case ROLE_LIST_SUCCESS:
			return {...state, roles: action.payload.rolesList};
		default:
			return state;
	}
};
export default settings;
