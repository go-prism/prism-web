import {GetRefractInfoResponse} from "@prism/prism-rpc/build/gen/service/reactor/api_pb";
import {REMOTE_LIST_SUCCESS} from "../actions/remote/List";
import {REMOTE_GET_SUCCESS} from "../actions/remote/Get";
import {REFRACT_LIST_SUCCESS} from "../actions/refract/List";
import {DataActionType} from "../actions";
import {REFRACT_GET_SUCCESS} from "../actions/refract/Get";
import {REMOTE_LIST_ARCH_SUCCESS} from "../actions/remote/ListByArch";
import {RESET_REFRACTION, RESET_REFRACTIONS} from "../actions/refract";
import {RESET_REMOTE, RESET_REMOTES} from "../actions/remote";
import {CacheEntries, Refractions, RefractionV1, Remotes, RemoteV1} from "../../config/types";
import {CACHE_LIST_SUCCESS} from "../actions/cache/List";
import {RESET_CACHES} from "../actions/cache";
import {REFRACT_GET_INFO_SUCCESS} from "../actions/refract/GetInfo";
import {GET_FILE_REQUEST, GET_FILE_RESET, GET_FILE_SUCCESS} from "../actions/core";

export interface DataState {
	remotes: Remotes;
	remote: RemoteV1 | null;
	refractions: Refractions;
	refraction: RefractionV1 | null;
	refractInfo: GetRefractInfoResponse.AsObject | null;
	cacheEntries: CacheEntries;
	filePreview: string | null;
}

const initialState: DataState = {
	remotes: [],
	remote: null,
	refractions: [],
	refraction: null,
	refractInfo: null,
	cacheEntries: [],
	filePreview: null
};

const data = (state = initialState, action: DataActionType): DataState => {
	switch (action.type) {
		case GET_FILE_RESET:
		case GET_FILE_REQUEST:
			return {...state, filePreview: null};
		case GET_FILE_SUCCESS:
			return {...state, filePreview: action.payload};
		case RESET_REMOTE:
			return {...state, remote: null};
		case RESET_REMOTES:
			return {...state, remotes: []};
		case REMOTE_LIST_ARCH_SUCCESS:
		case REMOTE_LIST_SUCCESS:
			return {...state, remotes: action.payload.remotesList || []};
		case REMOTE_GET_SUCCESS:
			return {...state, remote: action.payload};
		case RESET_REFRACTION:
			return {...state, refraction: null, refractInfo: null};
		case RESET_REFRACTIONS:
			return {...state, refractions: []};
		case REFRACT_LIST_SUCCESS:
			return {...state, refractions: action.payload.refractionsList || []};
		case REFRACT_GET_SUCCESS:
			return {...state, refraction: action.payload};
		case REFRACT_GET_INFO_SUCCESS:
			return {...state, refractInfo: action.payload};
		case RESET_CACHES:
			return {...state, cacheEntries: []};
		case CACHE_LIST_SUCCESS:
			return {...state, cacheEntries: action.payload.entriesList || []};
		default:
			return state;
	}
};
export default data;
