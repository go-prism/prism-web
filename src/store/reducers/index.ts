import {combineReducers} from "redux";
import generic from "./generic";
import data from "./data";
import errors from "./errors";
import loading from "./loading";
import settings from "./settings";

const rootReducers = combineReducers({
	generic,
	data,
	settings,
	errors,
	loading
});

export type TState = ReturnType<typeof rootReducers>;
export default rootReducers;
