import React, {ReactNode} from "react";
import Icon from "@mdi/react";
import {mdiDebian, mdiServer} from "@mdi/js";
import {Theme} from "@material-ui/core";

export const getRemoteIcon = (theme: Theme, type: string): ReactNode => {
	switch (type) {
		case "helm":
		case "alpine":
		case "maven":
		case "node":
		case "golang":
			return <img
				src={`${process.env.PUBLIC_URL}/${type}_logo.svg`}
				alt={`${type} logo`}
				width={24}
				height={24}
			/>
		case "debian":
			return <Icon
				path={mdiDebian}
				color={theme.palette.error.dark}
				size={1}
			/>
		default:
			return <Icon
				path={mdiServer}
				color={theme.palette.text.secondary}
				size={1}
			/>
	}
}